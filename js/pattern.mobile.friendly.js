function Pattern($root) {
  var self = this;
  self.start = 0;
  self.end = 0;
  self.$root = $root;
  self.started = false;
  self.dots = {startIndex: 0, length: 9};
  self.savedPattern = [];
  self.pattern = [];
  self.isNewPattern = true;
  self.message = document.getElementById('message');
  self.lines = {};
  self.log = document.getElementById('draggable');
  var i = self.dots.startIndex;
  while (i < self.dots.length) {
    var new_node = self.getNewNode('button', i);
    $root.appendChild(new_node);
    self.dots[i] = new_node;
    i++
  }
}

Pattern.prototype.updateLog = function (x, y) {
  var self = this;
  // self.log.innerText = 'X: ' + x + '; Y: ' + y;
  var realTarget = document.elementFromPoint(x, y);
  if (realTarget.classList.contains('button')) {
    realTarget.style.background = "#00ffff";
    var id = realTarget.id;
    self.start = id;
    if (self.pattern.indexOf(id) == -1) {
      self.pattern.push(id);
    }
  }
};

Pattern.prototype.getNewNode = function (className, id) {
  var new_node = document.createElement('div');
  new_node.className = className;
  new_node.id = id;
  if (className != 'dot') {
    new_node.appendChild(this.getNewNode('dot', ''));
  }
  return new_node
};

Pattern.prototype.clear = function () {
  var self = this;
  var i = self.dots.startIndex;
  while (i < self.dots.length) {
    self.dots[i].style.background = "#EFEFEF";
    i++;
  }
  for (var line_ids in self.lines) {
    self.lines[line_ids].style.visibility = "hidden";
    self.lines[line_ids].style.zIndex = "-1";
  }
  self.message.innerText = "Draw pattern to unlock";
  self.pattern = [];
  self.message.click();
};

Pattern.prototype.show = function () {
  var self = this;
  var line_id = 'line-' + self.start + self.end;
  var line = document.getElementById(line_id);
  if (self.end < self.start) {
    line_id = 'line-' + self.end + self.start;
    line = document.getElementById(line_id);
  }
  if (line) {
    line.style.visibility = "visible";
    line.style.zIndex = "2";
    self.lines[line_id] = line;
  }
};

Pattern.prototype.touchStart = function (id) {
  if (event) {
    event.preventDefault();
  }
  var self = this;
  self.started = true;
  self.start = id;
  if (self.pattern.indexOf(id) == -1) {
    self.pattern.push(id);
  }
  self.clear();
  if (event) {
    event.currentTarget.style.background = "#00ffff"
  }
};

Pattern.prototype.updateMessage = function (self) {
  if (self.isNewPattern) {
    self.savedPattern = self.pattern.slice(0);
    self.message.innerText = "Successfully created new pattern " + self.savedPattern;
  } else {
    if (self.matchPattern(self.savedPattern, self.pattern)) {
      self.message.innerText = "Matched with saved pattern";
    } else {
      self.message.innerText = "Invalid pattern";
    }
  }
};

Pattern.prototype.touchOver = function (id) {
  event.preventDefault();
  var self = this;

  if (self.started) {
    if (self.end != id) {
      self.start = self.end;
    }
    self.end = id;
    if (self.pattern.indexOf(self.end) == -1) {
      self.pattern.push(self.end);
    }
    self.updateMessage(self);
    event.currentTarget.style.background = "#00ffff";
    self.show();
  }
  if (id == null) {
    self.updateMessage(self);
  }
};

Pattern.prototype.touchEnd = function () {
  var self = this;
  event.preventDefault();
  self.started = false;
  event.currentTarget.style.background = "#00ffff";
  document.activeElement.blur();
  self.isNewPattern = false;
  setTimeout(function () {
    self.clear();
  }, 2000);
};

Pattern.prototype.matchPattern = function (a, b) {
  var i = a.length;
  if (i != b.length) return false;
  while (i--) {
    if (a[i] !== b[i]) return false;
  }
  return true;
};

Pattern.prototype.init = function () {
  var self = this;
  var i = self.dots.startIndex;
  while (i < self.dots.length) {
    (function (i) {
      var id = String(i);
      var dot = self.dots[id];
      dot.onmousedown = self.touchStart.bind(self, id);
      dot.onmouseover = self.touchOver.bind(self, id);
      dot.onmouseup = self.touchEnd.bind(self, id);

      dot.addEventListener('touchstart', function (e) {
        self.started = true;
        self.updateLog(e.changedTouches[0].pageX, e.changedTouches[0].pageY);
      }, false);

      dot.addEventListener('touchmove', function (e) {
        e.preventDefault();
        self.updateLog(e.targetTouches[0].pageX, e.targetTouches[0].pageY);
      }, false);

      dot.addEventListener("touchend", function (e) {
        e.preventDefault();
        self.started = false;
        self.touchOver(null);
        self.touchEnd();
      }, false);

    })(i++);
  }
  document.ontouchmove = function (event) {
    event.preventDefault();
  };
};

var p = new Pattern(document.getElementById("buttonGrid"));
p.init();

